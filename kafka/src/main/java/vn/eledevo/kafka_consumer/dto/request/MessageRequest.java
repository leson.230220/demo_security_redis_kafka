package vn.eledevo.kafka_consumer.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class MessageRequest {
    private String method;
    private EmailRequest value;
}

