package vn.eledevo.kafka_consumer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import vn.eledevo.kafka_consumer.dto.request.MessageRequest;
import vn.eledevo.kafka_consumer.dto.response.UserResponse;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    @Value("${application.kafka.user-topic-response}")
    private String userTopicResponse;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final KafkaTemplate<String, UserResponse> userResponseKafkaTemplate;

    @KafkaListener(topics = "userEledevo", groupId = "sonGroup")
    public void getUser(String message) throws JsonProcessingException {
        MessageRequest messageRequest = objectMapper.readValue(message, MessageRequest.class);
        System.out.println("Da vao duoc day" +  messageRequest);
        UserResponse userResponse = new UserResponse()
                .builder()
                .id(1L)
                .email(messageRequest.getValue().getEmail())
                        .lastname("Sown")
                                .firstname("Son")
                                        .password("123456")
                                                .build();
        userResponseKafkaTemplate.send(userTopicResponse, userResponse);
        System.out.println("Send Response nguoc lai");

    }
}
