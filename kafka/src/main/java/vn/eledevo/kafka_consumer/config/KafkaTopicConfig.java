package vn.eledevo.kafka_consumer.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Value("${application.kafka.user-topic-response}")
    private String userTopicResponse;

    @Bean
    public NewTopic userTopicResponse() {
        return TopicBuilder
                .name(userTopicResponse)
                .build();
    }
}
