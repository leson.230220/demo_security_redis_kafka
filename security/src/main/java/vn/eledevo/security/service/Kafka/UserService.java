package vn.eledevo.security.service.Kafka;

import org.springframework.stereotype.Service;
import vn.eledevo.security.dto.request.EmailRequest;
import vn.eledevo.security.dto.request.RegisterRequest;
import vn.eledevo.security.dto.response.UserResponse;

@Service
public interface UserService {
    UserResponse sendUserRequest (EmailRequest emailRequest);
}
