package vn.eledevo.security.service.Kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import vn.eledevo.security.dto.request.EmailRequest;
import vn.eledevo.security.dto.request.MessageRequest;
import vn.eledevo.security.dto.request.RegisterRequest;
import vn.eledevo.security.dto.response.UserResponse;
import vn.eledevo.security.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

@Component
@Slf4j
@RequiredArgsConstructor

public class UserServiceImpl implements UserService{
    @Value("${application.kafka.user-topic}")
    private String userTopic;

    private final KafkaTemplate<String, MessageRequest> kafkaTemplate;
    private final ObjectMapper objectMapper = new ObjectMapper();

    private List<UserResponse> userResponseList = new ArrayList<>();

    @Override
    public UserResponse sendUserRequest(EmailRequest emailRequest) {
        MessageRequest messageRequest = new MessageRequest("GET", emailRequest);
        kafkaTemplate.send(userTopic ,messageRequest);
        System.out.println("Send message thành công");

        return userResponseList.getFirst();
    }

    @KafkaListener (topics = "userEledevoResponse", groupId = "sonGroup")
    public void userConsumer(String msg) throws JsonProcessingException {
        UserResponse userResponse = objectMapper.readValue(msg, UserResponse.class);
        userResponseList.add(userResponse);
    }
}
