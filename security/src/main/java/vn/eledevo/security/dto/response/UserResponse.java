package vn.eledevo.security.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import vn.eledevo.security.constant.Role;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class UserResponse {
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String password;
//    private Role role;
}
