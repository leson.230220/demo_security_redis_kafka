package vn.eledevo.security.controller.userKafkaController;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import vn.eledevo.security.dto.request.EmailRequest;
import vn.eledevo.security.dto.request.RegisterRequest;
import vn.eledevo.security.dto.response.UserResponse;
import vn.eledevo.security.service.Kafka.UserService;

@RestController
@RequestMapping("api/v1/kafka")
@RequiredArgsConstructor
public class UserKafkaController {

    private final UserService userService;

    @GetMapping("")
    public ResponseEntity<UserResponse> getUserByKafka (@RequestBody EmailRequest emailRequest){
        return ResponseEntity.ok().body(userService.sendUserRequest(emailRequest));
    }

}
