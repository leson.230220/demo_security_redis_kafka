package vn.eledevo.security.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import static org.springframework.http.HttpMethod.*;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;
import static vn.eledevo.security.constant.Permission.*;
import static vn.eledevo.security.constant.Role.ADMIN;
import static vn.eledevo.security.constant.Role.MANAGER;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableMethodSecurity
public class SecurityConfiguration {
    // Tạo ra 1 mảng kiểu String chứa các đường dẫn API cho phép truy cập mà không cần quyền (permitAll)
    private static final String[] WHITE_LIST_URL = {"/api/v1/auth/**",
            "/api/v1/kafka/**",
            "/v2/api-docs",
            "/v3/api-docs",
            "/v3/api-docs/**", // ** là trỏ tất cả các method bên trong API này
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui/**",
            "/webjars/**",
            "/swagger-ui.html"};
    private final JwtAuthenticationFilter jwtAuthFilter;
    private final AuthenticationProvider authenticationProvider;
    private final LogoutHandler logoutHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http
                .csrf(AbstractHttpConfigurer::disable) // Đây là 1 phương pháp bảo mật không dùng nên để disable
                .authorizeHttpRequests(req ->
                                req.requestMatchers(WHITE_LIST_URL) // nhận mảng API ở trên cho phép không cần quyền
                                        .permitAll()
                                .requestMatchers("/api/v1/management/**").hasAnyRole(ADMIN.name(), MANAGER.name()) //api này phải có role là admin hoặc manager dưới tương tự
                                .requestMatchers(GET, "/api/v1/management/**").hasAnyAuthority(ADMIN_READ.name(), MANAGER_READ.name())
                                .requestMatchers(POST, "/api/v1/management/**").hasAnyAuthority(ADMIN_CREATE.name(), MANAGER_CREATE.name())
                                .requestMatchers(PUT, "/api/v1/management/**").hasAnyAuthority(ADMIN_UPDATE.name(), MANAGER_UPDATE.name())
                                .requestMatchers(DELETE, "/api/v1/management/**").hasAnyAuthority(ADMIN_DELETE.name(), MANAGER_DELETE.name())
                                        .anyRequest()
                                        .authenticated() // tất cả các request được authenticated thì sẽ được truy cập vào với các api không có requestMatchers là mặc định permitAll
                )
                .sessionManagement(session -> session.sessionCreationPolicy(STATELESS)) // Mỗi 1 request là riêng biệt không liên quan gì đến nhau
                .authenticationProvider(authenticationProvider) //Để xác thực người dùng trong lần đăng nhập
                .addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class) // Mỗi request đều được điều hướng đến doFilter để kiểm tra
                .logout(logout -> // kiểm tra xem api có phải là api logout
                        logout.logoutUrl("/api/v1/auth/logout")
                                .addLogoutHandler(logoutHandler) // Thêm vào 1 hành động trước khi logout cụ thể hành động được implemt trong class LogoutService
                                .logoutSuccessHandler((request, response, authentication) -> SecurityContextHolder.clearContext()) // định nghĩa là đăng xuất thành công và xóa bỏ thông tin người dùng trong phiên đăng nhập trong SecurityContextHolder
                )
        ;

        return http.build();
    }
}
