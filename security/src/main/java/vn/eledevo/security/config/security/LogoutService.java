package vn.eledevo.security.config.security;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Service;
import vn.eledevo.security.repository.TokenRepository;
import vn.eledevo.security.repository.redisRepository.RedisRepository;

@Service
@RequiredArgsConstructor
public class LogoutService implements LogoutHandler {

  private final JwtService jwtService;
  private final RedisRepository redisRepository;
  // Nếu API là logout thì sẽ chạy hàm logout
  @Override
  public void logout(
      HttpServletRequest request,
      HttpServletResponse response,
      Authentication authentication
  ) {
    final String authHeader = request.getHeader("Authorization"); // Lấy ra header từ request
    final String jwt;
    // Kiểm tra xem header có null hoặc bắt đầu bằng chuỗi Bearer không
    if (authHeader == null ||!authHeader.startsWith("Bearer ")) {
      return;
    }
    // Nếu header tồn tại và đúng định dạng sẽ bỏ qua 7 ký tự đầu tiên
    jwt = authHeader.substring(7); // Cắt chuỗi header từ ký tự index số 7 trở đi ( "Bearer " )
    var userEmail = jwtService.extractUsername(jwt);
    if (!redisRepository.keyExists(userEmail) || redisRepository.get(userEmail) != jwt){
      return;
    }
      redisRepository.delete(userEmail);
      SecurityContextHolder.clearContext();  // Xóa thông tin phiên đăng nhập trước đó có trong ContextHolder
    }
  }
